#Homework 3

This branch contains the third homework of the Digital Image Processing course. It is still in development.
As it goes it will contains multiple implementation of the Fourier transform and filters in the frequency domain.

##Instructions

To compile this project, you need to include some libraries:

- The project is based on open CV: http://opencv.org/ you will need to download the development kit.
- To parse command line arguments I choosed to use a small library: http://tclap.sourceforge.net/ 

### Static Lib

The program use static linking, to include static libraries to the project, drop them in "/Homework3/staticlib".

To make it compile you need to copy the "opencv\build\x86\vc10\staticlib" directory to "/Homework3/staticlib".
But to make it easier, here is the list the opencv libs the project use for debug:

- opencv_core2410d.lib
- opencv_highgui2410d.lib
- libtiffd.lib
- libpngd.lib
- libjpegd.lib
- libjasperd.lib
- IlmImfd.lib
- zlibd.lib
- Vfw32.lib
- comctl32.lib

And here is the list of the opencv libs used by the project in release:

- opencv_highgui2410.lib
- libtiff.lib
- libpng.lib
- libjpeg.lib
- libjasper.lib
- IlmImf.lib
- zlib.lib
- Vfw32.lib
- comctl32.lib

### Includes

To add any dependencies to the project just create a new directory inside "Homework3/include". Visual Studio is configured to look for it inside
this folder.

The project use TCLAP and opencv, so you will need to copy:

- "opencv\build\include\opencv2" to "Homework3/include/opencv2"
- "tclap-1.2.1\include\tclap\" to "Homework3/include/tclap"