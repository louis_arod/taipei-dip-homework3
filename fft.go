package main
 
import (
    "fmt"
    "math"
    "math/cmplx"
)
 
type CV_64FC2 struct {
	re float64 
	im float64 
}
 
func _ditfft2(x []float64, y []CV_64FC2, n, s int) {
    if n == 1 {
        y[0].re = x[0]
		y[0].im = 0
        return
    }
    _ditfft2(x, y, n/2, 2*s)
    _ditfft2(x[s:], y[n/2:], n/2, 2*s)
    for k := 0; k < n/2; k++ {
        tf := CV_64FC2{}
		
		cosk := math.Cos(-2*math.Pi*float64(k)/float64(n))
		sink := math.Sin(-2*math.Pi*float64(k)/float64(n))
		tf.re = cosk * y[k+n/2].re - sink * y[k+n/2].im
		tf.im = sink * y[k+n/2].re + cosk * y[k+n/2].im 
		
		fmt.Printf("(%f %fi)\n", tf.re, tf.im)
		
		y[k+n/2].re = y[k].re - tf.re
		y[k+n/2].im = y[k].im - tf.im
		
		y[k].re += tf.re
		y[k].im += tf.im
    }
}

func ditfft2(x []float64, y []complex128, n, s int) {
    if n == 1 {
        y[0] = complex(x[0], 0)
        return
    }
    ditfft2(x, y, n/2, 2*s)
    ditfft2(x[s:], y[n/2:], n/2, 2*s)
    for k := 0; k < n/2; k++ {
        tf := cmplx.Rect(1, -2*math.Pi*float64(k)/float64(n)) * y[k+n/2]
		
		fmt.Printf("(%f %fi)\n", real(tf), imag(tf))
        
		y[k], y[k+n/2] = y[k]+tf, y[k]-tf
    }
}
 
func main() {
    x := []float64{1, 1, 1, 1, 0, 0, 0, 0}
    y := make([]complex128, len(x))
	z := make([]CV_64FC2, len(x))
    ditfft2(x, y, len(x), 1)
	fmt.Println("")
	_ditfft2(x,z,len(x), 1)
    fmt.Println("")
	for _, c := range y {
        fmt.Printf("%8.4f | %8.4f\n", c, cmplx.Abs(c))
    }
	fmt.Println("")
	for _, c := range z {
        fmt.Printf("(%f %fi)\n", c.re, c.im)
    }
}