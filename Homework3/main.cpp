#include <cstdio>
#include <tclap\CmdLine.h>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include "error.h"
#include "exercices.h"

using namespace std;
using namespace cv;

struct Arguments {
	string inputFile;
	
	Error parseArgs(int argc, char **argv);
};

int main (int argc, char** argv)
{
	Arguments args;
	Error err = Error::OK;

	err = args.parseArgs(argc, argv);
	if (err != Error::OK) {
		cin.get();
		return Error::ERROR;
	}

	cout << args.inputFile << endl;
	Mat img = imread(args.inputFile, CV_LOAD_IMAGE_GRAYSCALE);
	if (!img.data) {
		cerr << "File not found at: \"" + args.inputFile + "\"\n";
		cin.get();
		return Error::OPEN;
	}

	return exercice22(img);
}

Error Arguments::parseArgs(int argc, char **argv)
{
	try {
		TCLAP::CmdLine cmd("Program for the 2014 DIP class.", ' ', "0.1");
		TCLAP::UnlabeledValueArg<string> inputFileArg("image", "Input file.", true, "", "string");
		cmd.add(inputFileArg);
		cmd.parse(argc, argv);

		this->inputFile = inputFileArg.getValue();
		
	} catch (TCLAP::ArgException &e) {
		cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		return Error::PARAM;
	}
	return Error::OK;
}