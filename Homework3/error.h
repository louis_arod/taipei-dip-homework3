#pragma once

// HACK change if possible...
#undef ERROR
#undef OVERFLOW

// Enumerate possible errors.
// Enum scope problem: http://stackoverflow.com/a/2506286
struct Error {
    enum Type {
        OK          =    0,
        ERROR       =   -1,
        READ        =   -2,
		OPEN		=	-3,
		PARAM		=	-4,
		OVERFLOW	=	-5
    };
    Type t_;
    Error(Type t) : t_(t) {}
    operator Type () const { return t_; }
};