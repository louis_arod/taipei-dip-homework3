#pragma once

#include <opencv2/core/core.hpp>
#include "Error.h"

class Homework2
{
public:
	// histogram draw the histogram from input to output with the given color.
	// output size must be greater than 0.
	static Error histogram(const cv::Mat &input, cv::Mat &output, uchar color);
	// logarithm apply the logarithm transformation to img.
	static Error logarithm(cv::Mat &img, double c);
	// powerlaw apply the power-law transformation to img using the power p.
	static Error powerlaw(cv::Mat &img, double p);
	// piecewise apply a piece-wise linear transformaton on img using filter.
	// The filter must be a grayscale image of size 256x256.
	static Error piecewise(cv::Mat &img, const cv::Mat &filter);
	// histoEq apply the histogram-equalization algorithm on img.
	static Error histoEq(cv::Mat &img);
	// histoSpe do histogram-specification of img using inputHisto.
	// The histogram must be 256px wide.
	static Error histoSpe(cv::Mat &img, cv::Mat &inputHisto);
	
	static void laplacianSharpening(cv::Mat &img, const float kernel[3][3]);

	template<typename T> static void convolution(cv::Mat &img, const float kernel[3][3])
	{
		const Mat original = img.clone();

		CV_Assert(img.depth() != sizeof(T));

		for (int y = 1; y < img.rows - 1; y++) {
			for (int x = 1; x < img.cols - 1; x++) {
				float sum = 0;
				// We do the convolution for 1 px.
				for(int k = -1; k <= 1; k++){
					for(int j = -1; j <=1; j++){
						sum += kernel[j + 1][k + 1] * original.at<T>(y - j, x - k);
					}
				}
				img.at<T>(y,x) = (T)sum;
			}
		}
	}
	template<typename T> static void scale(cv::Mat &img, float factor = -1)
	{
		T min = img.at<T>(0);
		T max = img.at<T>(0);
		T *raw = (T*)img.data;

		CV_Assert(img.depth() != sizeof(T));

		// Find min.
		for (int i = 0; i < img.rows; i++) {
			for (int j = 0; j < img.cols; j++) {
				T tmp = raw[i * img.cols + j];
				if (tmp < min) {
					min = tmp;
				} else if (tmp > max) {
					max = tmp;
				}
			}
		}

		// Compute new max.
		max -= min;

		// Scale.
		float coef = 255.0f / (float)max;
		if (factor != -1) {
			coef = factor / (float)max;
		}
		for (int i = 0; i < img.rows; i++) {
			raw = img.ptr<T>(i);
			for (int j = 0; j < img.cols; j++) {
				T tmp = raw[j];
				raw[j] = (T)((tmp - min) * coef);
			}
		}
	}
private:
	// readHisto read an histogram from an image.
	static void computeHisto(const cv::Mat &img, double histo[256]);
	// equalizeHisto equalize an histogram.
	static void equalizeHisto(double histo[256]);
	// parseHisto convert a B&W image histogram to an array.
	static Error parseHisto(const cv::Mat &img, double histo[256]);
};

