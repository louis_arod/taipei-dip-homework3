#include <cstdio>
#include <opencv2\core\core.hpp>
#include "homework3.h"

using namespace cv;
using namespace homework3;

void testHW3_1()
{
	const size_t data_h = 4, data_w = 4;
	float data[][4] = {
		{0, 1, 2, 3},
		{1, 2, 3, 4},
		{2, 3, 4, 5},
		{3, 4, 5, 6},
	};
	Mat arr = Mat(data_h, data_w, CV_32FC1, &data);
	Mat fft;
	Mat ifft;
	Mat magPha;
	Mat fancy;

	homework3::center(arr);

	homework3::fft(arr, fft);
	homework3::fft(fft, ifft, FFT_INVERSE|FFT_SCALE);
	
	homework3::cmplxToMagPha(fft, magPha);
	homework3::cmplxToDisplay(fft, fancy);

	//dft(arr, fft, DFT_COMPLEX_OUTPUT);
	//dft(fft, ifft, DFT_INVERSE|DFT_REAL_OUTPUT|DFT_SCALE);

	printf("\n--     FFT     --\n");
	for (int i = 0; i < fft.rows; i++) {
		for (int j = 0; j < fft.cols; j++) {
			Vec2f cmplx = fft.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	printf("\n--  Mag Pha  --\n");
	for (int i = 0; i < magPha.rows; i++) {
		for (int j = 0; j < magPha.cols; j++) {
			Vec2f cmplx = magPha.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	printf("\n-- Fancy Mag --\n");
	for (int i = 0; i < fancy.rows; i++) {
		for (int j = 0; j < fancy.cols; j++) {
			printf("(%+2.4f) ", fancy.at<float>(i,j));
		}
		printf("\n");
    }

	printf("\n-- Inverse FFT --\n");
	for (int i = 0; i < ifft.rows; i++) {
		for (int j = 0; j < ifft.cols; j++) {
			Vec2f cmplx = ifft.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	getchar();
}

void testHW3_2()
{
	const size_t data_h = 4, data_w = 4;
	float data[][4] = {
		{0, 1, 2, 3},
		{1, 2, 3, 4},
		{2, 3, 4, 5},
		{3, 4, 5, 6},
	};
	Mat arr = Mat(data_h, data_w, CV_32FC1, &data);
	Mat fft_hw3, fft_cv;
	Mat ifft_hw3, ifft_cv;
	Mat magPha;
	Mat fancy;

	homework3::center(arr);

	homework3::fft(arr, fft_hw3);
	homework3::fft(fft_hw3, ifft_hw3, FFT_INVERSE|FFT_SCALE);

	dft(arr, fft_cv, DFT_COMPLEX_OUTPUT);
	dft(fft_cv, ifft_cv, DFT_INVERSE|DFT_SCALE);

	printf("\n--   FFT HW3   --\n");
	for (int i = 0; i < fft_hw3.rows; i++) {
		for (int j = 0; j < fft_hw3.cols; j++) {
			Vec2f cmplx = fft_hw3.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	printf("\n--   FFT CV    --\n");
	for (int i = 0; i < fft_cv.rows; i++) {
		for (int j = 0; j < fft_cv.cols; j++) {
			Vec2f cmplx = fft_cv.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	printf("\n--   IFFT HW3  --\n");
	for (int i = 0; i < ifft_hw3.rows; i++) {
		for (int j = 0; j < ifft_hw3.cols; j++) {
			Vec2f cmplx = ifft_hw3.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }
	printf("\n--   IFFT CV   --\n");
	for (int i = 0; i < ifft_cv.rows; i++) {
		for (int j = 0; j < ifft_cv.cols; j++) {
			Vec2f cmplx = ifft_cv.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }
	getchar();
}

void testHW3_3()
{
	const size_t data_h = 4, data_w = 4;
	float data[][4] = {
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 10, 11, 12},
		{13, 14, 15, 16},
	};
	Mat arr = Mat(data_h, data_w, CV_32FC1, &data);
	Mat fft_hw3, fft_cv;
	Mat ifft_hw3, ifft_cv;
	Mat magPha;
	Mat fancy;

	homework3::center(arr);

	homework3::fft(arr, fft_hw3);
	homework3::fft(fft_hw3, ifft_hw3, FFT_INVERSE|FFT_SCALE);

	dft(arr, fft_cv, DFT_COMPLEX_OUTPUT);
	dft(fft_cv, ifft_cv, DFT_INVERSE|DFT_SCALE);

	printf("\n--   FFT HW3   --\n");
	for (int i = 0; i < fft_hw3.rows; i++) {
		for (int j = 0; j < fft_hw3.cols; j++) {
			Vec2f cmplx = fft_hw3.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	printf("\n--   FFT CV    --\n");
	for (int i = 0; i < fft_cv.rows; i++) {
		for (int j = 0; j < fft_cv.cols; j++) {
			Vec2f cmplx = fft_cv.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	printf("\n--   IFFT HW3  --\n");
	for (int i = 0; i < ifft_hw3.rows; i++) {
		for (int j = 0; j < ifft_hw3.cols; j++) {
			Vec2f cmplx = ifft_hw3.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }
	printf("\n--   IFFT CV   --\n");
	for (int i = 0; i < ifft_cv.rows; i++) {
		for (int j = 0; j < ifft_cv.cols; j++) {
			Vec2f cmplx = ifft_cv.at<Vec2f>(i,j);
			printf("(%+2.4f, %+2.4f) ", cmplx[0], cmplx[1]);
		}
		printf("\n");
    }

	getchar();
}