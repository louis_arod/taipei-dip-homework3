/**
 * This file contains the source of a simple FFT transform.
 * This FFT is not memory efficient but easy to read.
 */
#include <opencv2\core\core.hpp>

using namespace cv;
namespace simplefft
{

// FFT flags.
enum {
		FFT_NOFLAG	= 0x01,
		FFT_SCALE	= 0x02,
		FFT_INVERSE	= 0x04,
	};

static const float PI = 3.141592653589793238460f;

// findClosestPowOf2 find the closest power of two number to v.
static inline int findClosestPowOf2(int v)
{
	int pow = 0;
	bool powOf2 = v && !(v & (v - 1));
	
	while (v >>= 1) // unroll for more speed...
	{
		pow++;
	}
	// If not a power of 2 we add one.
	if (!powOf2) {
		pow++;
	}
	return pow;
}

// mul_f multiply the channel number 'chan' of a Mat by a scalar.
static inline void mul_f(Mat &img, float scalar, int chan) {
	for (int i = 0; i < img.rows; i++) {
		Vec2f *pxs = img.ptr<Vec2f>(i);
		for (int j = 0; j < img.cols; j += 2) {
			pxs[j][chan] *= scalar;
			pxs[j + 1][chan] *= scalar;
		}
	}
}

void copyMakeBorder(const Mat &img, Mat &dst, int *pow = NULL)
{
	size_t p = 0, n = 0;
	size_t max = MAX(img.cols, img.rows);

	p = findClosestPowOf2(max);
	n = 1 << p;

	dst = Mat::zeros(n, n, CV_32F);

	for (int i = 0; i < img.rows; i++) {
		const float *pix = img.ptr<float>(i);
		float *pxs = dst.ptr<float>(i);
		for (int j = 0; j < img.cols; j++) {
			pxs[j] = pix[j];
		}
	}

	if (pow != NULL) {
		*pow = p;
	}
}

void _fft(MatConstIterator_<Vec2f> img, MatIterator_<Vec2f> out, int n, int step)
{
	if (n == 1) {
		out[0][0] = img[0][0];
		out[0][1] = img[0][1];
		return;
	}
	_fft(img, out, n / 2, 2 * step);
	_fft(img + step, out + (n / 2), n / 2, 2 * step);
	for (int k = 0; k < (n / 2); k++) {
		Vec2f outK = out[k];
		Vec2f outKN = out[k + n / 2];
		
		float cosk = cos(-2 * PI * k / n);
		float sink = sin(-2 * PI * k / n);
		float re = cosk * outKN[0] - sink * outKN[1];
		float im = sink * outKN[0] + cosk * outKN[1];

		//printf("(%f %fi)\n", re, im);

		outKN[0] = outK[0] - re;
		outKN[1] = outK[1] - im;	
		outK[0]  = outK[0] + re;
		outK[1]  = outK[1] + im;

		out[k] = outK;
		out[k + n / 2] = outKN;
	}
}
void fft(const Mat &img, Mat &transform, char flag)
{
	Mat tmps[2];
	Mat tmp;
	
	// Init tmp
	if ((flag & FFT_INVERSE) != 0) {
		// Since we already got a complex image,
		// we clone it and get its transpose.
		tmp = img.clone().t();
		// We conjugate the imaginary part.
		mul_f(tmp, -1, 1);
	} else {
		// We add padding to the input image.
		copyMakeBorder(img, tmps[0]);
		tmps[1] = Mat::zeros(tmps[0].rows, tmps[0].cols, CV_32F);
		// We create a image with two channels. (Complex image)
		merge(tmps, 2, tmp);
	}

	// Init transform
	transform = Mat::zeros(tmp.rows, tmp.cols, CV_32FC2);
	
	// Do the fft on line
	for (int i = 0; i < tmp.rows; i++) {
		MatConstIterator_<Vec2f> tmpRow = tmp.begin<Vec2f>() + i * tmp.cols;
		MatIterator_<Vec2f>  transformRow = transform.begin<Vec2f>() + i * tmp.cols;;
		_fft(tmpRow, transformRow , tmp.cols, 1);
	}

	// Do the fft on line
	transform.copyTo(tmp);
	tmp = tmp.t();
	for (int i = 0; i < tmp.rows; i++) {
		MatConstIterator_<Vec2f> tmpRow = tmp.begin<Vec2f>() + i * tmp.cols;
		MatIterator_<Vec2f>  transformRow = transform.begin<Vec2f>() + i * tmp.cols;;
		_fft(tmpRow, transformRow , tmp.cols, 1);
	}

	if ((flag & FFT_INVERSE) == 0) {
		// We return the transpose.
		transform = transform.t();
	}
	if ((flag & FFT_SCALE) != 0) {
		transform = transform / (transform.cols * transform.rows);
	}
}

} // namespace simplefft