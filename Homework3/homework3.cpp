#include <cmath>
#include <cfloat>
#include "homework3.h"

using namespace cv;

static const float PI = 3.141592653589793238460f;

// Written using http://stackoverflow.com/a/466242
static inline int findClosestPowOf2(int v)
{
	int pow = 0;
	bool powOf2 = v && !(v & (v - 1));
	
	while (v >>= 1) // unroll for more speed...
	{
		pow++;
	}
	// If not a power of 2 we add one.
	if (!powOf2) {
		pow++;
	}
	return pow;
}

static inline void mul_f(Mat &img, float scalar, int chan) {
	for (int i = 0; i < img.rows; i++) {
		Vec2f *pxs = img.ptr<Vec2f>(i);
		for (int j = 0; j < img.cols; j += 2) {
			pxs[j][chan] *= scalar;
			pxs[j + 1][chan] *= scalar;
		}
	}
}

void homework3::copyMakeBorder(const Mat &img, Mat &dst, int *pow)
{
	size_t p = 0, n = 0;
	size_t max = MAX(img.cols, img.rows);

	p = findClosestPowOf2(max);
	n = 1 << p;

	dst = Mat::zeros(n, n, CV_32F);

	for (int i = 0; i < img.rows; i++) {
		const float *pix = img.ptr<float>(i);
		float *pxs = dst.ptr<float>(i);
		for (int j = 0; j < img.cols; j++) {
			pxs[j] = pix[j];
		}
	}

	if (pow != NULL) {
		*pow = p;
	}
}

static void _fft(const Vec2f *img, Vec2f *out, int n, int step)
{
	if (n == 1) {
		out[0][0] = img[0][0];
		out[0][1] = img[0][1];
		return;
	}
	_fft(img, out, n / 2, 2 * step);
	_fft(img + step, out + (n / 2), n / 2, 2 * step);
	for (int k = 0; k < (n / 2); k++) {
		Vec2f outK = out[k];
		Vec2f outKN = out[k + n / 2];
		
		float cosk = cos(-2 * PI * k / n);
		float sink = sin(-2 * PI * k / n);
		float re = cosk * outKN[0] - sink * outKN[1];
		float im = sink * outKN[0] + cosk * outKN[1];

		//printf("(%f %fi)\n", re, im);

		outKN[0] = outK[0] - re;
		outKN[1] = outK[1] - im;	
		outK[0]  = outK[0] + re;
		outK[1]  = outK[1] + im;

		out[k] = outK;
		out[k + n / 2] = outKN;
	}
}

void homework3::fft(const Mat &img, Mat &transform, char flag)
{
	Mat tmp;
	Mat tmps[2];
	Vec2f *row;

	if ((flag & FFT_INVERSE) != 0) {
		ifft(img, transform, flag);
		return;
	}

	// Init transform
	homework3::copyMakeBorder(img, tmps[0]);
	tmps[1] = Mat::zeros(tmps[0].rows, tmps[0].cols, CV_32F);
	merge(tmps, 2, transform);

	// Allocate a row
	row = new Vec2f[transform.cols];

	// Init tmp
	tmp = Mat::zeros(transform.rows, transform.cols, CV_32FC2);
	
	// Do the fft on line
	for (int i = 0; i < transform.rows; i++) {
		const Vec2f *tRow = transform.ptr<Vec2f>(i);
		_fft(tRow, row, transform.cols, 1);

		// Store the result as column for the forward transform.
		for (int j = 0; j < tmp.rows; j += 2) {
			tmp.at<Vec2f>(j, i) = row[j];
			tmp.at<Vec2f>(j + 1, i) = row[j + 1];
		}
	}

	// Do the fft on line
	for (int i = 0; i < tmp.rows; i++) {
		const Vec2f *tmpRow = tmp.ptr<Vec2f>(i);
		_fft(tmpRow, row, tmp.cols, 1);

		// Store the result as column
		for (int j = 0; j < transform.rows; j += 2) {
			transform.at<Vec2f>(j, i) = row[j];
			transform.at<Vec2f>(j + 1, i) = row[j + 1];
		}
	}

	if ((flag & FFT_SCALE) != 0) {
		transform = transform / (transform.cols * transform.rows);
	}

	delete[] row;
}

void homework3::ifft(const Mat &img, Mat &transform, char flag)
{
	Mat tmp;
	Vec2f *row;

	// Init transform
	transform = img.clone();

	// Allocate a row
	row = new Vec2f[img.cols];

	// Init tmp
	tmp = Mat::zeros(transform.rows, transform.cols, CV_32FC2);
	
	// Read column and do ifft on it.
	for (int i = 0; i < tmp.rows; i++) {
		// Read a column of fft image.
		for (int j = 0; j < transform.rows; j += 2) {
			row[j] = transform.at<Vec2f>(j, i);
			row[j + 1] = transform.at<Vec2f>(j + 1, i);
			row[j][1] *= -1;
			row[j + 1][1] *= -1;
		}
		
		Vec2f *tRow = tmp.ptr<Vec2f>(i);
		_fft(row, tRow, tmp.cols, 1);
	}

	// Read column of the first pass ifft and do fft on it.
	for (int i = 0; i < transform.rows; i++) {
		// Read a column of ifft first pass.
		for (int j = 0; j < tmp.rows; j += 2) {
			row[j] = tmp.at<Vec2f>(j, i);
			row[j + 1] = tmp.at<Vec2f>(j + 1, i);
		}
		
		Vec2f *transformRow = transform.ptr<Vec2f>(i);
		_fft(row, transformRow, transform.cols, 1);
	}

	if ((flag & FFT_REAL) != 0) {
		Mat tmps[2];
		split(transform, tmps);
		transform = tmps[0];
	}
	if ((flag & FFT_SCALE) != 0) {
		transform = transform / (transform.cols * transform.rows);
	}

	delete[] row;
}

void homework3::cmplxToMagPha(const Mat &cmplx, Mat &magPha)
{
	magPha = Mat::zeros(cmplx.rows, cmplx.cols, CV_32FC2);

	for (int i = 0; i < cmplx.rows; i++) {
		const Vec2f *origin = cmplx.ptr<Vec2f>(i);
		Vec2f *trans = magPha.ptr<Vec2f>(i);
		for (int j = 0; j < cmplx.cols; j++) {
			float re = origin[j][0];
			float im = origin[j][1];
			trans[j][0] = sqrt(re * re + im * im);
			trans[j][1] = atan2(im, re);
		}
	}
}

void homework3::normalize(const Mat &img, Mat &norm, float min, float max)
{
	float minImg = img.at<float>(0);
	float maxImg = minImg;

	// Get min and max
	for (int i = 0; i < img.rows; i++) {
		const float *pxs = img.ptr<float>(i);
		for (int j = 0; j < img.cols; j++) {
			if (pxs[j] < minImg) {
				minImg = pxs[j];
			} else if (pxs[j] > maxImg) {
				maxImg = pxs[j];
			}
		}
	}

	if (norm.data != img.data) {
		norm = img.clone();
	}

	minImg -= min;
	float factor = max / (maxImg - minImg);

	norm -= minImg;
	norm *= factor;
}

void homework3::cmplxToDisplay(const Mat &cmplx, Mat &fancy)
{
	Mat tmps[2];

	cmplxToMagPha(cmplx, fancy);
	
	split(fancy, tmps);
	
	log(tmps[0], tmps[0]);
	homework3::normalize(tmps[0], fancy, 0, 1);
}

void homework3::center(Mat &img)
{
	for (int i = 0; i < img.rows; i++) {
		float *pxs = img.ptr<float>(i);
		for (int j = 0; j < img.cols; j++) {
			if ((i + j) % 2 != 0) {
				pxs[j] *= -1;
			}
		}
	}
}

void homework3::crop(const cv::Mat &origin, cv::Mat &ifft)
{
	ifft = ifft(Rect(0, 0, origin.rows, origin.cols));
}

void homework3::convolution_f(const Mat &src, Mat &dst, const Mat &kern, float divider, float delta)
{
	Mat tmp = src;
	int kernHalf = kern.cols/2;
	float scale = 1.0;

	// We that the kernel is a square and that it has odd number of elements.
	assert(kern.cols == kern.rows && kern.cols % 2 != 0);

	// If the source and the destination are the same,
	if (src.data == dst.data) {
		// We clone the source in tmp.
		tmp = src.clone();
	} else {
		// Else we initialize the destination.
		dst = Mat::zeros(src.size(), src.depth());
	}

	// Compute scale factor.
	if (delta != 0) {
		float sp = 0, sm = 0;
		for (int i = 0; i < kern.rows; i++) {
			const float *row = kern.ptr<float>(i);
			for (int j = 0; j < kern.cols; j++) {
				if (row[j] < 0) {
					sm += -1 * row[j];
				} else {
					sp += row[j];
				}
			}
		}
		scale = 1 / (2 * MAX(sm, sp));
	}

	for (int y = kernHalf; y < tmp.rows - kernHalf; y++) {
		float *dRow = dst.ptr<float>(y);
		for (int x = kernHalf; x < tmp.cols - kernHalf; x++) {
			float sum = 0;
			// We do the convolution for 1 px.
			for(int i = -kernHalf; i <= kernHalf; i++){
				const float *kRow = kern.ptr<float>(i + kernHalf);
				float *tRow = tmp.ptr<float>(y - i);
				for(int j = -kernHalf; j < kernHalf; j++) {
					sum += kRow[j + kernHalf] * tRow[x - j];
				}
			}
			dRow[x] = sum / divider; 
			dRow[x] *= scale + (delta / 2.0f);
		}
	}
}

void homework3::gaussianFilter(const Mat &src, Mat &dst, float theta)
{
	float ci, cj, theta2, scale;
	
	if (dst.data != src.data) {
		dst = Mat(src.size(), CV_32FC2);
	}

	ci = src.rows / 2.0f;
	cj = src.cols / 2.0f;
	theta2 = theta * theta;
	scale = 1 / (2 * PI * theta2);
	for (int i = 0; i < src.rows; i++) {
		const Vec2f *sRow = src.ptr<Vec2f>(i);
		Vec2f *dRow = dst.ptr<Vec2f>(i);
		for (int j = 0; j < src.cols; j++) {
			float h2 = (ci - i) * (ci - i);
			float w2 = (cj - j) * (cj - j);

			dRow[j][0] = sRow[j][0] * scale * exp(-1 * (h2 + w2) / theta2);
			dRow[j][1] = sRow[j][1] * scale * exp(-1 * (h2 + w2) / theta2);
		}
	}
}

void homework3::idealLPFilter(const Mat &src, Mat &dst, float theta)
{
	float ci, cj, theta2;
	
	if (dst.data != src.data) {
		dst = src.clone();
	}

	ci = ((float)src.rows) / 2.0f;
	cj = ((float)src.cols) / 2.0f;
	theta2 = theta * theta;
	for (int i = 0; i < src.rows; i++) {
		const Vec2f *sRow = src.ptr<Vec2f>(i);
		Vec2f *dRow = dst.ptr<Vec2f>(i);
		for (int j = 0; j < src.cols; j++) {
			float h2 = (ci - i) * (ci - i);
			float w2 = (cj - j) * (cj - j);

			if (h2 + w2 > theta2) {
				dRow[j][0] = 0;
				dRow[j][1] = 0;
			}
		}
	}
}

void homework3::laplacianFilter(const cv::Mat &src, cv::Mat &dst)
{
	float ci, cj;
	
	if (dst.data != src.data) {
		dst = src.clone();
	}

	ci = ((float)src.rows) / 2.0f;
	cj = ((float)src.cols) / 2.0f;
	for (int i = 0; i < src.rows; i++) {
		const Vec2f *sRow = src.ptr<Vec2f>(i);
		Vec2f *dRow = dst.ptr<Vec2f>(i);
		for (int j = 0; j < src.cols; j++) {
			float h2 = (ci - i) * (ci - i);
			float w2 = (cj - j) * (cj - j);

			dRow[j] *= -(h2 + w2);
		}
	}
}