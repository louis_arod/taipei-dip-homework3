#include <cstdio>
#include <opencv2\highgui\highgui.hpp>
#include "homework2.h"
#include "homework3.h"
#include "exercices.h"

using namespace cv;
using namespace homework3;

Error warmUp(Mat &img) {
	Mat fancy;
	Mat tmps[2];
	
	printf("Compute Fourier's transform...\n");
	Mat fImage, fourierTransform;
	img.convertTo(fImage, CV_32F);
	homework3::center(fImage);
	homework3::fft(fImage, fourierTransform);
	//dft(fImage, fourierTransform, DFT_COMPLEX_OUTPUT);

	printf("Process image in frequential domain.\n");
	homework3::cmplxToDisplay(fourierTransform, fancy);

	printf("Compute inverse Fourier's transform...\n");
	Mat inverseTransform;
	homework3::fft(fourierTransform, inverseTransform, homework3::FFT_INVERSE | homework3::FFT_SCALE);
	//dft(fourierTransform, inverseTransform, DFT_SCALE|DFT_INVERSE);

	namedWindow("Input image", WINDOW_AUTOSIZE);
	imshow("Input image", img);
	namedWindow("FFT image", WINDOW_AUTOSIZE);
	imshow("FFT image", fancy);
	namedWindow("IFFT image", WINDOW_AUTOSIZE);
	split(inverseTransform, tmps);
	homework3::center(tmps[0]);
	homework3::normalize(tmps[0], inverseTransform, 0, 1);
	imshow("IFFT image", inverseTransform);

	waitKey(0);
	return Error::OK;
}

Error exercice21(Mat &img) {
	const size_t kern_w = 5;
	const size_t kern_h = 5;
	Mat spatial, fft, fftFiltered, frequential;
	Mat kern;
	Mat fImg;
	Mat tmp;
	// Gaussian kernel with theta=1
	float kernDatas[kern_h][kern_w] = 
	{
			{  1,  4,  7,  4,  1},
			{  4, 16, 26, 16,  4},
			{  7, 26, 41, 26,  7},
			{  4, 16, 26, 16,  4},
			{  1,  4,  7,  4,  1},
	};

	printf("---- Exercice 21 ----\n");
	img.convertTo(fImg, CV_32F);

	// Do the spacial filtering.
	printf("Do spacial filtering.\n");
	kern = Mat(kern_h, kern_w, CV_32F, &kernDatas);
	convolution_f(fImg, spatial, kern, 273);

	// Do the frequential filtering.
	printf("Compute Fourier transform...\n");
	center(fImg);
	homework3::fft(fImg, fft);
	center(fImg);

	printf("Do frequential filtering.\n");
	idealLPFilter(fft, fftFiltered, 128);

	printf("Compute inverse Fourier transform...\n");
	ifft(fftFiltered, frequential, FFT_SCALE | FFT_REAL);
	center(frequential);

	// Display images.
	namedWindow("Input image", WINDOW_AUTOSIZE);
	homework3::normalize(fImg, fImg, 0, 1);
	imshow("Input image", fImg);
	
	namedWindow("Spacial Filter", WINDOW_AUTOSIZE);
	homework3::normalize(spatial, spatial, 0, 1);
	imshow("Spacial Filter", spatial);

	namedWindow("FFT", WINDOW_NORMAL);
	homework3::cmplxToDisplay(fft, tmp);
	imshow("FFT", tmp);

	namedWindow("FFT Filtered", WINDOW_NORMAL);
	homework3::cmplxToDisplay(fftFiltered, tmp);
	imshow("FFT Filtered", tmp);

	namedWindow("Frequential Filter", WINDOW_AUTOSIZE);
	crop(fImg, frequential);
	homework3::normalize(frequential, frequential, 0, 1);
	imshow("Frequential Filter", frequential);

	waitKey(0);
	return Error::OK;
}

Error exercice22(Mat &img) {
	const size_t kern_h = 3, kern_w = 3;
	Mat spatial, fft, fftFiltered, frequential;
	Mat kern;
	Mat fImg;
	Mat tmp;
	float kernDatas[kern_h][kern_w] = 
	{
			{ -1, -1, -1},
			{ -1,  8, -1},
			{ -1, -1, -1},
	};
	float kernDatas1[kern_h][kern_w] = 
	{
			{  0, -1,  0},
			{ -1,  4, -1},
			{  0, -1,  0},
	};

	printf("---- Exercice 22 ----\n");
	img.convertTo(fImg, CV_32F);
	homework3::normalize(fImg, fImg, 0, 1);

	// Do the spacial filtering.
	printf("Do spacial filtering.\n");
	kern = Mat(kern_h, kern_w, CV_32F, &kernDatas);
	convolution_f(fImg, spatial, kern);
	//spatial = fImg.clone();
	//Homework2::convolution<float>(spatial, kernDatas);

	// Do the frequential filtering.
	printf("Compute Fourier transform...\n");
	center(fImg);
	homework3::fft(fImg, fft);
	center(fImg);

	printf("Do frequential filtering.\n");
	laplacianFilter(fft, fftFiltered);

	printf("Compute inverse Fourier transform...\n");
	ifft(fftFiltered, frequential, FFT_SCALE | FFT_REAL);
	center(frequential);

	// Display images.
	namedWindow("Input image", WINDOW_AUTOSIZE);
	imshow("Input image", fImg);
	
	namedWindow("Spacial Filter", WINDOW_AUTOSIZE);
	homework3::normalize(fImg + spatial, spatial, 0, 1);
	imshow("Spacial Filter", spatial);

	namedWindow("FFT", WINDOW_NORMAL);
	homework3::cmplxToDisplay(fft, tmp);
	imshow("FFT", tmp);

	namedWindow("FFT Filtered", WINDOW_NORMAL);
	homework3::cmplxToDisplay(fftFiltered, tmp);
	imshow("FFT Filtered", tmp);

	namedWindow("Frequential Filter", WINDOW_AUTOSIZE);
	crop(fImg, frequential);
	homework3::normalize(frequential, frequential, 0, 1);
	//homework3::normalize(fImg + frequential, frequential, 0, 1);
	imshow("Frequential Filter", fImg - frequential);

	waitKey(0);
	return Error::OK;
}