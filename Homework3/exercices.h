#pragma once

#include <opencv2\core\core.hpp>
#include "error.h"

Error warmUp(cv::Mat &img);
Error exercice21(cv::Mat &img);
Error exercice22(cv::Mat &img);