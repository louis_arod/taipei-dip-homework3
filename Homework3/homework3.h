#pragma once

#include <opencv2\core\core.hpp>
#include "error.h"

namespace homework3
{
	enum {
		FFT_NOFLAG	= 0x01,
		FFT_SCALE	= 0x02,
		FFT_INVERSE	= 0x04,
		FFT_REAL	= 0x08,
	};
	
	void copyMakeBorder(const cv::Mat &img, cv::Mat &cmplxImg, int *pow = NULL);
	
	void fft(const cv::Mat &img, cv::Mat &transform, char flag = FFT_NOFLAG);
	void ifft(const cv::Mat &img, cv::Mat &transform, char flag = FFT_NOFLAG);
	
	void cmplxToMagPha(const cv::Mat &cmplx, cv::Mat &magPha);
	
	void normalize(const cv::Mat &img, cv::Mat &norm, float min, float max);
	
	void center(cv::Mat &img);

	void crop(const cv::Mat &origin, cv::Mat &ifft);
	
	/// @brief Return the magnitude of a complex image normalize on a logarithmic scale.
	void cmplxToDisplay(const cv::Mat &cmplx, cv::Mat &fancy);

	void convolution_f(const cv::Mat &src, cv::Mat &dst, const cv::Mat &kern, float divider = 1.0, float delta = 0.0);

	void gaussianFilter(const cv::Mat &src, cv::Mat &dst, float theta);
	void idealLPFilter(const cv::Mat &src, cv::Mat &dst, float theta);
	void laplacianFilter(const cv::Mat &src, cv::Mat &dst);
}