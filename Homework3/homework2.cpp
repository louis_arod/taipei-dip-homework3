#include <cmath>
#include "Homework2.h"

using namespace cv;

Error Homework2::histogram(const cv::Mat &input, cv::Mat &output, uchar color)
{
	const unsigned int pxCountSize = 256;

	Error err = Error::OK;
	unsigned int pxCount[pxCountSize] = {0}; // Put all the elements to 0. http://stackoverflow.com/a/1065800

	// Check params.
	if (output.rows <= 0 || output.cols <= 0) {
		return Error::PARAM;
	}

	// Fill pixel count array from image.
	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			uchar px = input.at<uchar>(i * input.cols + j);
			pxCount[px]++;
		}
	}

	// Find the max from pxCount.
	unsigned int maxPxCount = 0;
	for (int i = 0; i < pxCountSize; i++) {
		if (maxPxCount < pxCount[i]) {
			maxPxCount = pxCount[i];
		}
	}

	// Plot histogram.
	// We scale the y axis.
	float maxHeight = 0.9f * output.rows; // Let a 10% margin-top.
	float coefY = maxPxCount / maxHeight;

	// We scale the x axis.
	float pasX = (float)output.cols / pxCountSize;

	for (int i = 0; i < output.rows; i++) {
		for (int j = 0; j < output.cols; j++) {
			int x = (int)(j / pasX);
			int y = (int)pxCount[x];
			y = (int)(y / coefY);

			if (i >= output.rows - y) {
				output.at<uchar>(i * output.cols + j) = color;
			}
		}
	}

	return Error::OK;
}

Error Homework2::logarithm(Mat &img, double c)
{
	uchar maxIntensity = 0;
	uchar *raw = (uchar*)img.data;

	if (img.rows <= 0 || img.cols <= 0) {
		return Error::PARAM;
	}

	// Search for the maximal pixel intensity.
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j ++) {
			uchar px = raw[i * img.rows + j];
			if (px > maxIntensity) {
				maxIntensity = px;
			}
		}
	}

	if (c <= 0) {
		c = 255.0 / (log(1.0 + maxIntensity));
	}

	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j ++) {
			uchar px = raw[i * img.rows + j];
			raw[i * img.rows + j] = (uchar)(c * (log(1.0 + px)));
		}
	}

	return Error::OK;
}

Error Homework2::powerlaw(Mat &img, double p)
{
	double c;
	uchar maxIntensity = 0;
	uchar *raw = (uchar*)img.data;

	if (img.rows <= 0 || img.cols <= 0) {
		return Error::PARAM;
	}

	// Search for the maximal pixel intensity.
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j ++) {
			uchar px = raw[i * img.rows + j];
			if (px > maxIntensity) {
				maxIntensity = px;
			}
		}
	}

	c = 255.0 / pow(maxIntensity, p);
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j ++) {
			uchar px = raw[i * img.rows + j];
			raw[i * img.rows + j] = (uchar)(c * pow(px, p));
		}
	}

	return Error::OK;
}

Error Homework2::piecewise(cv::Mat &img, const cv::Mat &filter)
{
	const int tableSize = 256;
	double table[tableSize];
	Error err = Error::OK;
	uchar *raw = (uchar*)img.data;

	if (filter.rows != 256 || filter.cols != 256) {
		return Error::PARAM;
	}

	// Read the filter from file.
	err = Homework2::parseHisto(filter, table);
	if (err != Error::OK) {
		return err;
	}

	// Let's apply the tansformation to the input image.
	for(int i = 0; i < img.rows; i++) {
		raw = img.ptr<uchar>(i);
		for(int j = 0; j < img.cols; j++) {
			uchar px = raw[j];
			raw[j] = (int)table[px];
		}
	}

	return Error::OK;
}

// Explanation found on http://fr.wikipedia.org/wiki/%C3%89galisation_d%27histogramme
Error Homework2::histoEq(Mat &img)
{
	const int histoEqSize = 256;
	double histoEq[histoEqSize] = {0};
	uchar *raw = (uchar*)img.data;

	Homework2::computeHisto(img, histoEq);
	Homework2::equalizeHisto(histoEq);

	// Apply transformation.
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			uchar px = raw[i * img.cols + j];
			raw[i * img.cols + j] = (uchar)histoEq[px];
		}
	}

	return Error::OK;
}

// Specification code http://fr.scribd.com/doc/106790597/OPENCV-Topic-04-Histogram-Specification
// Explanation found on http://fourier.eng.hmc.edu/e161/lectures/contrast_transform/node3.html
Error Homework2::histoSpe(Mat &img, Mat &inputHisto)
{
	const unsigned int imgHistoEqSize = 256;
	const unsigned int mapSize = 256;
	const unsigned int userHistoEqSize = 256;

	double imgHistoEq[imgHistoEqSize] = {0};
	double userHistoEq[userHistoEqSize] = {0};
	double map[mapSize] = {0};

	uchar *raw = (uchar*)img.data;

	Homework2::computeHisto(img, imgHistoEq);
	Homework2::equalizeHisto(imgHistoEq);

	Error err = Homework2::parseHisto(inputHisto, userHistoEq);
	if (err != Error::OK) {
		return err;
	}
	Homework2::equalizeHisto(userHistoEq);

	// We search the closest value from imgHistoEq[i] into userHistoEq.
	// Since the histograms are equalized they are strictly crescent.
	for (int i = 0, j = 0; i < mapSize; i++) {
		double value = imgHistoEq[i];
		bool mapped = false;

		while (!mapped) {
			if (value <= userHistoEq[j]) {
				mapped = true;
				map[i] = j;
			}
			j++;
		}
		j--;
	}

	// Apply transformation.
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			uchar px = raw[i * img.cols + j];
			raw[i * img.cols + j] = (uchar)map[px];
		}
	}

	return Error::OK;
}

void Homework2::computeHisto(const cv::Mat &img, double histo[256])
{
	uchar *raw = (uchar*) img.data;

	// Compute histogram.
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			uchar px = raw[i * img.cols + j];
			histo[px]++;
		}
	}
}

void Homework2::equalizeHisto(double histo[256]) {
	const unsigned int histoSize = 256;

	// Compute transformation table.
	for (int i = 1; i < histoSize; i++) {
		histo[i] = histo[i - 1] + histo[i];
	}

	double c = (256.0 - 1.0) / histo[histoSize - 1];
	for (int i = 0; i < histoSize; i++) {
		//histo[i] = (int)(ceil(c * histo[i]));
		histo[i] = c * histo[i];
	}
}

Error Homework2::parseHisto(const Mat &img, double histo[256])
{
	enum { BLACK = 0 };
	const unsigned int histoSize = 256;
	double imgDepth = img.rows - 1;

	if (img.cols != 256) {
		return Error::PARAM;
	}

	// Set all element to the max rows. (For every pixel in input we put the value 0).
	for (int i = 0; i < histoSize; i++) {
		histo[i] = imgDepth;
	}

	// Fill the array. (We search for the smalest BLACK pixel)
	const uchar *raw = img.data;
	for(int i = 0; i < img.rows; i++) {
		for(int j = 0; j < img.cols; j++) {
			if (raw[i * img.rows + j] == BLACK && i < histo[j]) {
				histo[j] = i;
			}
		}
	}

	// Now table contains an inverted value type (255 - V).
	// Let's fix that.
	for (int i = 0; i < histoSize; i++) {
		histo[i] = imgDepth - histo[i];
	}

	return Error::OK;
}

// Laplacian enhancement: http://www.idlcoyote.com/ip_tips/sharpen.html
void Homework2::laplacianSharpening(cv::Mat &img, const float kernel[3][3])
{
	Mat contour;
	Mat imgFloat;

	// Get float matrix.
	img.convertTo(imgFloat, CV_32F);
	contour = imgFloat.clone();
	
	Homework2::convolution<float>(contour, kernel);
	Homework2::scale<float>(contour);

	// Substract between contour and imgFloat.
	for (int i = 0; i < imgFloat.rows; i++) {
		for (int j = 0; j < imgFloat.cols; j++) {
			imgFloat.at<float>(i * imgFloat.cols + j) += contour.at<float>(i * contour.cols + j);
		}
	}

	Homework2::scale<float>(imgFloat);
	imgFloat.convertTo(img, CV_8U);
}

/*template<typename T> void Homework2::scale(cv::Mat &img)
{
	T min = img.at<T>(0);
	T max = img.at<T>(0);
	T *raw = (T*)img.data;

	CV_Assert(img.depth() != sizeof(T));

	// Find min.
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			T tmp = raw[i * img.cols + j];
			if (tmp < min) {
				min = tmp;
			} else if (tmp > max) {
				max = tmp;
			}
		}
	}

	// Compute new max.
	max -= min;

	// Scale.
	float coef = 255.0f / (float)max;
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			T tmp = raw[i * img.cols + j];
			raw[i * img.cols + j] = (T)((tmp - min) * coef);
		}
	}
}*/

/*template<typename T> void Homework2::convolution(Mat &img, const float kernel[3][3])
{
	const Mat original = img.clone();

	CV_Assert(img.depth() != sizeof(T));

    for (int y = 1; y < img.rows - 1; y++) {
        for (int x = 1; x < img.cols - 1; x++) {
            float sum = 0;
			// We do the convolution for 1 px.
            for(int k = -1; k <= 1; k++){
                for(int j = -1; j <=1; j++){
                    sum += kernel[j + 1][k + 1] * original.at<T>(y - j, x - k);
                }
            }
            img.at<T>(y,x) = (T)sum;
        }
    }
}*/